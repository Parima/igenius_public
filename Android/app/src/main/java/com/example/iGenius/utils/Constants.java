package com.example.iGenius.utils;

/**
 * Created by Parima on 21/07/2017.
 */
public class Constants {

    public static final String BASE_URL = "https://api.instagram.com";
    public static final String IG_CLIENT_ID = "45963345a4ae42e2af78fe30750c5802";
    public static final String IG_CLIENT_SECRET = "35fc10a86d89479b924442f5666301bf";
    public static final String IG_REDIRECT_URL = "https://www.igenius.net/";
    public static final String IG_AUTH_URL = "https://api.instagram.com/oauth/authorize/";
    public static final String IG_TOKEN_URL = "https://api.instagram.com/oauth/access_token";
    public static final String AUTORISATION_CODE = "authorization_code";

    public static final String TAG_WEBVIEW_SERVICE = "WebViewService";
    public static final String TAG_MainActivity = "MainActivity";
    public static final String TAG_GalleryFragment = "GalleryFragment";
}
