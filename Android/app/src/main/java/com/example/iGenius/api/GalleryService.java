package com.example.iGenius.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Parima on 21/07/2017.
 */
public interface GalleryService {


    @GET("/v1/users/self/media/recent")
    Call<ResponseBody> getResponse(@Query("access_token") String accessToken,
                                   @Query("max_id") String maxId, @Query("min_id") String minId);
}