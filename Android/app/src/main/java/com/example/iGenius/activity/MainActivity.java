package com.example.iGenius.activity;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import com.example.iGenius.R;
import com.example.iGenius.api.AuthenticationServiceGenerator;
import com.example.iGenius.api.WebViewService;
import com.example.iGenius.model.Authentication;
import com.example.iGenius.utils.Constants;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Parima on 27/02/17.
 */

public class MainActivity extends Activity {

    private java.lang.String mUrl = "https://api.instagram.com/oauth/authorize/?client_id=" +
            Constants.IG_CLIENT_ID + "&redirect_uri=" + Constants.IG_REDIRECT_URL + "&response_type=code&display=touch&scope=public_content";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getAccessTokenCode();

    }

    private void getAccessTokenCode() {
        final WebView mWebView = (WebView) findViewById(R.id.webview);

        WebViewService webViewService = new WebViewService(this, new WebViewService.AuthenticationListener() {
            @Override
            public void onSuccess(String response) {
                Log.d(Constants.TAG_MainActivity, "TADA response " + response);
                mWebView.setVisibility(View.GONE);
                showLoading();

                doLogin(response);

            }

            @Override
            public void onFailure(String error) {
                hideLoading();
            }
        });

        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.setWebViewClient(webViewService);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(mUrl);

    }

    private void doLogin(String code) {

        final Call<Authentication> accessToken =  AuthenticationServiceGenerator.createTokenService().getAccessToken
                (Constants.IG_CLIENT_ID,Constants.IG_CLIENT_SECRET,Constants.IG_REDIRECT_URL,Constants.AUTORISATION_CODE,code);
        accessToken.enqueue(new Callback<Authentication>() {
            @Override
            public void onResponse(Call<Authentication> call, Response<Authentication> response) {

                if(response.isSuccessful()){
                    String authToken =  response.body().getAccess_token();
                    Log.d(Constants.TAG_MainActivity, "YOOHOO response " + authToken);
                    findViewById(R.id.webview).setVisibility(View.INVISIBLE);
                    GalleryFragment newFragment = new GalleryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("AUTH_TOKEN", authToken);
                    newFragment.setArguments(bundle);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.rl, newFragment,
                            "HELLO");
                    ft.addToBackStack(null).commit();

                }else{
                    try {

                        Log.d(Constants.TAG_MainActivity, "BOOOOO response " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                hideLoading();

            }


            @Override
            public void onFailure(Call<Authentication> call, Throwable t) {

                hideLoading();

            }
        });

    }

    public void showLoading(){

        findViewById(R.id.loading).setVisibility(View.VISIBLE);
    }

    public void hideLoading(){

        findViewById(R.id.loading).setVisibility(View.GONE);
    }

}
