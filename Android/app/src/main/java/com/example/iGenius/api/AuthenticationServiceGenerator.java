package com.example.iGenius.api;

import com.example.iGenius.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Parima on 21/07/2017.
 */
public class AuthenticationServiceGenerator {

    public static AuthenticationService createTokenService(){
        return getRetrofit().create(AuthenticationService.class);
    }

    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
