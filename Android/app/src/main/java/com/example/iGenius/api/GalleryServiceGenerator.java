package com.example.iGenius.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Parima on 21/07/2017.
 */
public class GalleryServiceGenerator {

    public static GalleryService createService() {
        return getRetrofit().create(GalleryService.class);
    }



    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("https://api.instagram.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
