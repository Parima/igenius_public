package com.example.iGenius.api;

import com.example.iGenius.model.Authentication;
import com.example.iGenius.model.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Parima on 21/07/2017.
 */
public interface AuthenticationService {

    @FormUrlEncoded @POST("/oauth/access_token")
    Call<Authentication> getAccessToken(@Field("client_id") String client_id,
                                        @Field("client_secret") String client_secret,
                                        @Field("redirect_uri") String redirect_uri,
                                        @Field("grant_type") String grant_type,
                                        @Field("code") String code);
}
