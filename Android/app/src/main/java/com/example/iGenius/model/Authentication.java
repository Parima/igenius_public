package com.example.iGenius.model;

/**
 * Created by Parima on 21/07/2017.
 */
public class Authentication {

    private String client_id;
    private String client_secret;
    private String authorization_code;
    private String redirect_url;
    private String code;


    private User user;

    private String access_token;

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }

    public String getAccess_token ()
    {
        return access_token;
    }

    public void setAccess_token (String access_token)
    {
        this.access_token = access_token;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [user = "+user+", access_token = "+access_token+"]";
    }

}
