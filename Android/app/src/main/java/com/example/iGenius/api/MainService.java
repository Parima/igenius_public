package com.example.iGenius.api;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Parima on 27/02/17.
 */
public class MainService {

    private static final String TAG = "CAService";

    private static MainService _instance;

    private Context context;

    public static MainService getInstance(Context context) {
        if (_instance == null) {
            _instance = new MainService(context);
        }
        return _instance;

    }

    private MainService(Context context) {
        this.context = context;

    }

    // the generic interface for a call with onSuccess and onFailure result
    public interface RequestListener {
        void onSuccess(Object response);
        void onFailure(String error);
    }


    public String getBaseServiceURL() {
        return "";
    }

    public void readAllBrands(final RequestListener listener) {

        performGetRequest(getBaseServiceURL(), listener);
    }

    private void performGetRequest(String url, final RequestListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(90000); // 90 seconds


        client.get(url, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                // no listener: no call back
                if (listener == null)
                    return;

                // no response: its an error
                if (response == null)
                    listener.onFailure("No DATA");

                // check for error tag in response
                boolean hasError = response.optBoolean("error");
                if (hasError) {
                    String title = response.optString("title");
                    String detail = response.optString("detail");
                    String errorMessage = MainService.formatErrorTypeAndMessage(title, detail);
                    listener.onFailure(errorMessage);
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e) {
                super.onFailure(statusCode, headers, responseBody, e);
                if (listener != null)
                    if (e.getLocalizedMessage()!=null)
                        listener.onFailure(e.getLocalizedMessage());
                    else
                        listener.onFailure( e.getClass().toString() );
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, e, errorResponse);
                if (listener != null) {
                    if (e.getLocalizedMessage()!=null)
                        listener.onFailure(e.getLocalizedMessage());
                    else
                        listener.onFailure( e.getClass().toString() );
                }
            }
        });

    }

    public static String formatErrorTypeAndMessage(String type, String message){
        return type + ": " + message;
    }


}