package com.example.iGenius.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.iGenius.utils.Constants;

/**
 * Created by Parima on 21/07/2017.
 */
public class WebViewService extends WebViewClient {

    private AuthenticationListener mListener;
    private Context context;

    public WebViewService(Context context, AuthenticationListener mListener) {
        this.context = context;
        this.mListener = mListener;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith(Constants.IG_REDIRECT_URL)) {
            String urls[] = url.split("=");
            mListener.onSuccess(urls[1]);
            return true;
        }
        return false;
    }

    @Override
    public void onReceivedError(WebView view, int errorCode,
                                String description, String failingUrl) {
        Log.d(Constants.TAG_WEBVIEW_SERVICE, "Page error: " + description);

        super.onReceivedError(view, errorCode, description, failingUrl);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        Log.d(Constants.TAG_WEBVIEW_SERVICE, "Loading URL: " + url);

        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        Log.d(Constants.TAG_WEBVIEW_SERVICE, "onPageFinished URL: " + url);
    }

    // the generic interface for a call with onSuccess and onFailure result
    public interface AuthenticationListener {
        void onSuccess(String response);
        void onFailure(String error);
    }
}
