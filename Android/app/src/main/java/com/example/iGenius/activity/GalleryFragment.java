package com.example.iGenius.activity;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Picture;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.iGenius.R;
import com.example.iGenius.adapter.PhotoAdapter;
import com.example.iGenius.api.GalleryServiceGenerator;
import com.example.iGenius.model.Photo;
import com.example.iGenius.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Parima on 21/07/2017.
 */
public class GalleryFragment extends Fragment {

    List<Photo> mPictures;
    PhotoAdapter mAdapter;
    RecyclerView mRecyclerView;
    String mAuthToken;
    private String mMaxId, mMinId;
    private String mQuery="";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mAuthToken = getArguments().getString("AUTH_TOKEN");
        mPictures = new ArrayList<Photo>();
        mAdapter = new PhotoAdapter(mPictures);
        mRecyclerView.setAdapter(mAdapter);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(manager);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {

                    final int visibleThreshold = 2;

                    GridLayoutManager layoutManager = (GridLayoutManager) mRecyclerView.getLayoutManager();
                    int lastItem = layoutManager.findLastCompletelyVisibleItemPosition();
                    int currentTotalCount = layoutManager.getItemCount();

                    if (currentTotalCount <= lastItem + visibleThreshold) {
                        //show your loading view
                        // load content in background

                        getTagResults(mQuery, "", "");

                    }
                }

            }
        });


        getTagResults(mQuery, "", "");
        return rootView;
    }

    private void getTagResults(String query,final String minId,final String maxId) {


        Thread thread = new Thread(new Runnable(){
            @Override
            public void run(){
                Call<ResponseBody> response = GalleryServiceGenerator.createService().getResponse(mAuthToken, minId, maxId);
                response.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {




                            StringBuilder sb = new StringBuilder();
                            try {
                                BufferedReader reader = new BufferedReader(new InputStreamReader(response.body().byteStream()));
                                String line;

                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }

                                JSONObject tagResponse = new JSONObject(sb.toString());

                                for (int i = 0; i < tagResponse.length() - 2; i++) {
                                    JSONObject pagination = tagResponse.getJSONObject("pagination");

                            //        mMaxId = pagination.getString("next_max_id");
                            //        mMinId = pagination.getString("next_min_id");

                            //        JSONObject meta = tagResponse.getJSONObject("meta");
                                    JSONArray data = tagResponse.getJSONArray("data");

                                    for (int j = 0; j < data.length(); j++) {

                            //            JSONArray tags = data.getJSONObject(j).getJSONArray("tags");


                                        JSONObject images = data.getJSONObject(j).getJSONObject("images").getJSONObject("low_resolution");


                                        Photo picture = new Photo();
                                        picture.setURL(images.getString("url"));
                                        mPictures.add(picture);
                                        Log.d(Constants.TAG_GalleryFragment, "*** url " + images.getString("url"));

                                        getActivity().runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {
                                                mAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    }

                                }




                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }


                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        t.printStackTrace();
                    }

                });
            }
        });
        thread.start();



    }
}
